var swissKnifeControllers = angular.module('swissKnifeControllers', []);

swissKnifeControllers.controller('mainCtrl',["$scope",
	function($scope){}]);

swissKnifeControllers.controller('dictionaryCtrl',
	["$scope","$rootScope","$routeParams","TranslateChn","TranslateEng","$location","$anchorScroll","$filter","$compile", "Restangular","$window","$document",
	function($scope,$rootScope,$routeParams,TranslateChn,TranslateEng, $location, $anchorScroll, $filter, $compile, Restangular, $window, $document){
	$scope.fromLang = "Chinese";
	$scope.toLang = "English";

	//switch language
	$scope.langExchange=function(){
		var validate, tmp=$scope.fromLang;
		$scope.fromLang=$scope.toLang;
		$scope.toLang=tmp;
		if (tmp==="Chinese"){
			tmp="ischn";
			validate="iseng";
		}
		else{
			validate="ischn";
			tmp="iseng";
		}
		$scope.currentPage = 1;
		$scope.digest;
	}

	//Submit
	$scope.raw;
	$scope.master = {};
	$scope.currentPage = 1;
	$scope.predicate = "cht"; //default sort
	$scope.orderReverse = "false";

	$scope.mySplit = function(string) {
	    var array = string.split(',');
	    return array;
	}

	$scope.update = function(user,queryAll) { 
	//user=user input , queryAll==0 -> not querrying all , queryAll==currentPage+1 -> query all
    	$scope.master.kw = user;
    	var apiUrl,
 			queryString,
 			queryUrl;

    	if ($scope.fromLang==="Chinese"){
			queryString = '?kw=' + $scope.master.kw + '&page=' + parseInt($scope.currentPage-queryAll) + '&order=' + $scope.predicate + '&reverse=' + $scope.orderReverse;
			apiUrl='http://1.34.137.108:3000/tranEng/';
	    }
	    else if ($scope.fromLang==="English"){
			queryString = '?kw=' + $scope.master.kw + '&page=' + parseInt($scope.currentPage-queryAll) + '&order=' + $scope.predicate + '&reverse=' + $scope.orderReverse;
			apiUrl = 'http://1.34.137.108:3000/tranChn/';
	    }
	    queryUrl = apiUrl + queryString;
	   	Restangular.allUrl('raw',queryUrl).getList().then(function(raw){
		   	$scope.totalItems=raw[0];
		   	raw.shift();
		   	if ($scope.totalItems > 0){
			   	$scope.showedTranslates = raw;
			   	for(var i=0;i<$scope.showedTranslates.length;i++){
			   		$scope.showedTranslates[i].eng=$scope.mySplit($scope.showedTranslates[i].eng);
			   	}
			}
			//if query all, do infinite scroll. 20 results added and showed at a time
			else if($scope.totalItems==-1){
				if(!$scope.showedTranslates){
					$scope.showedTranslates=[];
				}
				//first 20 results
				for(var i=0;i<20;i++){
					$scope.showedTranslates[i]=raw[i];	
					$scope.showedTranslates[i].eng=$scope.mySplit($scope.showedTranslates[i].eng);
				}
				var infiniteScr=1;
				var loadMore=function(){
					if($scope.showedTranslates.length < raw.length - 20){
    					if(this.pageYOffset + this.screen.availHeight >= $document.height()) {
	       					for(var i = infiniteScr*20 ; i < (infiniteScr + 1) * 20 ; i++){
	       						$scope.showedTranslates[i]=raw[i];
	             				$scope.showedTranslates[i].eng=$scope.mySplit($scope.showedTranslates[i].eng);
	             			}
	             			infiniteScr++;
   						}
   					}
   					else{
   						if(this.pageYOffset + this.screen.availHeight >= $document.height()) {
	       					for(var i = infiniteScr*20 ; i < raw.length ; i++){
	       						$scope.showedTranslates[i]=raw[i];
	             				$scope.showedTranslates[i].eng=$scope.mySplit($scope.showedTranslates[i].eng);
	             			}
	             			angular.element($window).off("scroll", loadMore);
   						}
   					}
   					$scope.$apply();
				};
				angular.element($window).on("scroll", loadMore);
			}
		   	$scope.loading=false;
	    });
	   	$scope.loading=true;
	   	$scope.showedTranslates=null;
  	};
  	

	// Sort
	var orderBy = $filter('orderBy');
  	$scope.order = function(predicate) {
	    $scope.orderReverse = ($scope.predicate === predicate) ? !$scope.orderReverse : false;
	    $scope.predicate = predicate;
	    $scope.currentPage = 1; //if order changed, go back to page 1
	    $scope.update($scope.master.kw,0);
	};

	//detect window size
  	$scope.getWidth = function() {
        return $(window).width();
    };
    $scope.$watch($scope.getWidth, function(newValue, oldValue) {
        $scope.window_width = newValue;
        $scope.maxSize = $scope.window_width > 768 ? 5 : 3;
    });
    window.onresize = function(){
        $scope.$apply();
    }

	// Pagination
  	$scope.itemsPerPage = 5;

}]);


swissKnifeControllers.controller('calculatorCtrl', function ($scope) {
	$scope.inputNum=[];
	$scope.inputOps=[];
	$scope.displayNum=0;
	$scope.records=[];
	$scope.buttons = [7,8,9,"+",4,5,6,"-",1,2,3,"X","C",0,"=","/"];
	var currentNum=0;
	$scope.apply_input=function(btn){

		if(btn>=0 && btn <=9){//input is number
			
			if (currentNum==0||null)
			{
				currentNum=btn;
			}
			else{
				currentNum=currentNum*10+btn;
			}

		}
		else if(btn=="C"){
				currentNum=0;
				$scope.inputNum=[];
				$scope.inputOps=[];
		}
		else if(currentNum!=null){//input is operation
			
			if (btn=="="){
				calculate();
			}
			else{
				$scope.inputOps.push(btn);
				if(currentNum==null){
					currentNum=0;
				}
				$scope.inputNum.push(currentNum);
				currentNum=null; //prevent consucutive operators
			}
		}
		$scope.displayNum="";//display current inputs
		 for (var i=0;i<$scope.inputNum.length;i++){
		 	$scope.displayNum+=$scope.inputNum[i];
		 	if($scope.inputOps[i]){$scope.displayNum+=$scope.inputOps[i];}
		 }
		 if($scope.inputNum.length==0||currentNum!=null){$scope.displayNum+=currentNum;}		
	};
	var calculate=function(){
		var result;
		var record;
		$scope.inputNum.push(currentNum);
		for(var i=0;i<$scope.inputNum.length;i++){
			if($scope.inputOps[i-1]){
				switch($scope.inputOps[i-1]){
					case "+": result+=$scope.inputNum[i];break;
					case "-": result-=$scope.inputNum[i];break;
					case "X": result*=$scope.inputNum[i];break;
					case "/": result/=$scope.inputNum[i];break;
				}
			}
			else{
				result=$scope.inputNum[i];
			}
			
		}
		currentNum=result;
		record=$scope.displayNum+"="+result;
		$scope.records.push(record);
		$scope.inputNum=[];
		$scope.inputOps=[];
	}
});