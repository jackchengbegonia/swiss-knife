var dictionaryServices = angular.module('dictionaryServices', ['ngResource']);

dictionaryServices.factory('TranslateChn', ['$resource',
  function($resource){
    // return $resource("http://jchw0106.herukuapp.com/tran/en/", {}, {
    	return $resource("https://www.moedict.tw/uni/:kw", {}, {
      query: {method:'GET', params:{}, isArray:true,cache:false, withCredentials :true}
    });
  }]);

dictionaryServices.factory('TranslateEng', ['$resource',
  // function($resource){
  //   // return $resource("http://jchw0106.herukuapp.com/tran/en/", {}, {
  //   	return $resource("http://www.dictionaryapi.com/api/v1/references/collegiate/xml/:kw", {}, {
  //     query: {method:'GET', params:{key:"b926111a-e0e0-4357-a0c6-562f8be365a3"}}
  //   });
  // }]);
function($resource){
    return $resource('http://1.34.137.108:8080/api/results',null, {
        'update': { method:'PUT' }
    }
    );
  }]);



