function printWeb() {
    window.print();
}

//toggle side menu

toggleSideMenu=function(){
    $("#wrapper").toggleClass("toggled");
    $("#menu-toggle").toggleClass("toggled");
    $(".sidebar-nav").toggleClass("toggled");
    // getStyleRuleValue('background')
}

$(document).ready(function(){
	$("#menu-toggle").click(toggleSideMenu);
	//if in mobile side menu auto collapse after menu item clicked/touched
	$(".sidebar-nav a").click(function(){
		if ($(window).width() <= 768){
			toggleSideMenu();
		}
	});
 });

$(window).load(function() {
    //fix for navbar height changed by theme
    $("body").css('padding-top' , $(".navbar-fixed-top").height());
    var css_old = document.styleSheets[2];
  	var ti = setInterval(function() {
    if (document.styleSheets[2] != css_old) {

      setTimeout(function(){ 
      	$("body").css('padding-top' , $(".navbar-fixed-top").height());
      }, 300);
      
    }
  }, 20);
});

// access the color of the theme
function getStyleRuleValue(style, selector, sheet) {
    var sheets = typeof sheet !== 'undefined' ? [sheet] : document.styleSheets;
    for (var i = 0, l = sheets.length; i < l; i++) {
        var sheet = sheets[i];
        if( !sheet.cssRules ) { continue; }
        for (var j = 0, k = sheet.cssRules.length; j < k; j++) {
            var rule = sheet.cssRules[j];
            if (rule.selectorText && rule.selectorText.split(',').indexOf(selector) !== -1) {
                return rule.style[style];
            }
        }
    }
    return null;
}


 