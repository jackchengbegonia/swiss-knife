var swissKnife = angular.module('swissKnife', [
  'ngRoute',
  'swissKnifeControllers',
  'restangular',
  'dictionaryServices',
  'ui.bootstrap',
  'swissKnifeAnimate',
  'smoothScroll',
  'sly'
]);

swissKnife.config(['$routeProvider', '$locationProvider', '$httpProvider', 'RestangularProvider',
	function($routeProvider, $locationProvider ,$httpProvider,RestangularProvider) {
		$routeProvider.
		when("/home",{
			templateUrl: "/partial/home.html",
			controller: ""
		}).
		when("/dictionary",{
			templateUrl: "/partial/dictionary.html",
			controller: "dictionaryCtrl"
		}).
		when("/calculator",{
			templateUrl: "/partial/calculator.html",
			controller: "calculatorCtrl"
		}).
		otherwise({
			redirectTo:"/home"
		});
		//this can get rid of the "#" in url but requires server side config
		// $locationProvider.html5Mode(true);

	}]);

//Validation
var ISENG_REGEXP = /^[a-zA-Z]+$/;
var NOTCHN_REGEXP = /[^\u4e00-\u9fa5]/;

swissKnife.directive('valida', function() {
	return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			attrs.$observe('valida', function(value){
			ctrl.$validators.valida = function(modelValue, viewValue) {
				if (ctrl.$isEmpty(modelValue)) {
          // consider empty models to be valid
          			return true;
      			}

      			if(value==="Chinese"&&!NOTCHN_REGEXP.test(viewValue)){ 
		          // it is valid
		         	return true;
	      		}
	      		else if(value==="English"&&ISENG_REGEXP.test(viewValue)){
	      			return true;
	      		}

        // it is invalid
        return false;
			};
		});
		}
	};
});

swissKnife.filter('highlight',function($sce){
	return function(text, phrase) {
           	if (phrase) text = text.replace(new RegExp('('+phrase+')', 'gi'),'<span class="highlighted">$1</span>');
            
            return $sce.trustAsHtml(text)
          }
});