/*
 * Module Dependencies
 */
var express = require('express');
var httpProxy = require('http-proxy');
var server = express();
server.set('port', 3000);
server.use(express.static(__dirname + '/app'));

//for ng-view routing
server.all('/hw/*', function(req, res){
    res.sendFile(__dirname + '/app/index.html');
    console.log("Request made to /hw/");
});

//proxy of api
// var apiForwardingUrl = 'http://dictionaryapi.net/api/definition/';
var apiForwardingUrl = 'http://jchw0106.herokuapp.com/tran/en/';
var option={changeOrigin: true};
var apiProxy = httpProxy.createProxyServer(option);
console.log('Forwarding API requests to ' + apiForwardingUrl);

server.all("/tranEng/*", function(req, res) {
	apiForwardingUrl = 'http://jchw0106.herokuapp.com/tran/en/';
	req.url=req.url.slice(9);
	console.log("url="+apiForwardingUrl+req.url);
    apiProxy.web(req, res, {target: apiForwardingUrl});
});
server.all("/tranChn/*", function(req, res) {
	apiForwardingUrl = 'http://jchw0106.herokuapp.com/tran/ch/';
	req.url=req.url.slice(9);
	console.log("url="+apiForwardingUrl+req.url);
    apiProxy.web(req, res, {target: apiForwardingUrl});
   // console.log(apiProxy.web(req, res, {target: apiForwardingUrl}));
});

server.listen(server.get('port'), function() {
    console.log('Express server listening on port ' + server.get('port'));
});