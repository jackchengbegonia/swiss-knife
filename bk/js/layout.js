function printWeb() {
    window.print();
}

//toggle side menu

toggleSideMenu=function(){
    $("#wrapper").toggleClass("toggled");
    $("#menu-toggle").toggleClass("toggled");
    $(".sidebar-nav").toggleClass("toggled");
    // getStyleRuleValue('background')
}

$(document).ready(function(){
	$("#menu-toggle").click(toggleSideMenu);
	//if in mobile side menu auto collapse after menu item clicked/touched
	$(".sidebar-nav a").click(function(){
		if ($(window).width() <= 768){
			toggleSideMenu();
		}
	});
 });

$(window).load(function() {
    //fix for navbar height changed by theme
    $("body").css('padding-top' , $(".navbar-fixed-top").height());
    var css_old = document.styleSheets[2];
  	var ti = setInterval(function() {
    if (document.styleSheets[2] != css_old) {

      setTimeout(function(){ 
      	$("body").css('padding-top' , $(".navbar-fixed-top").height());
      }, 300);
      
    }
  }, 20);
}); 