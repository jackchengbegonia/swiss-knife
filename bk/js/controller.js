var swissKnifeControllers = angular.module('swissKnifeControllers', []);

swissKnifeControllers.controller('mainCtrl',["$scope",
	function($scope){}]);

swissKnifeControllers.controller('dictionaryCtrl',
	["$scope","$routeParams","TranslateChn","TranslateEng","$location","$anchorScroll","$filter","$compile", "Restangular",
	function($scope,$routeParams,TranslateChn,TranslateEng, $location, $anchorScroll, $filter, $compile, Restangular){
	$scope.test=[];
	$scope.projects = Restangular.all("apple").getList().$object;
	$scope.testt=[
	{
		zhTW: "牟定1",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"AMouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定2",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"ZMouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定3",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"XMouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定4",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定5",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定6",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定7",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定8",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定9",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定10",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"CMouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定11",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定12",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定13",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"DMouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定14",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定15",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"YMouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定16",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定17",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定18",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"FMouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	},
	{
		zhTW: "牟定19",
		CN: "牟定",
		pron: "[Mou2 ding4]",
		ENdescr:"Mouding county in Chuxiong Yi autonomous prefecture",
		zhTWdescr: "楚雄彞族自治州",
		CNdescr: "楚雄彝族自治州",
		pinyin: "[Chu3 xiong2 Yi2 zu2 zi4 zhi4 zhou1]",
		loc: "Yunnan"
	}
	];
	$scope.fromLang="Chinese";
	$scope.toLang="English"

	//switch language
	$scope.langExchange=function(){
		var validate, tmp=$scope.fromLang;
		$scope.fromLang=$scope.toLang;
		$scope.toLang=tmp;
		if (tmp==="Chinese"){
			tmp="ischn";
			validate="iseng";
		}
		else{
			validate="ischn";
			tmp="iseng";
		}
		$scope.digest;
	}

	//Submit
	$scope.master = {};
	$scope.update = function(user) {
    	$scope.master.kw = user;
    	if ($scope.fromLang==="Chinese"){
	    	$scope.testresult=TranslateChn.get({kw:$scope.master.kw+".json"},function(){
			$scope.test = $scope.testresult.heteronyms[0].definitions;
	    	});
	    }
	    else if ($scope.fromLang==="English"){
	    	$scope.testresult=TranslateEng.get({kw:$scope.master.kw},function(){
		//	$scope.test = $scope.testresult.definitions;
	    	});
	    }
  	};

	// Sort
	var orderBy = $filter('orderBy');
	$scope.predicate = 'zhTW';
  	$scope.reverse = true;
  	$scope.order = function(predicate) {
	    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
	    $scope.predicate = predicate;
	    $scope.test = orderBy($scope.test, predicate, $scope.reverse);
	};

	// Pagination
	
  	$scope.currentPage = 1;
  	$scope.maxSize = 5;
  	$scope.itemsPerPage = 5;

  	var sliceResult=function(){
  		var begin = (($scope.currentPage - 1) * $scope.itemsPerPage)
  		, end = begin + $scope.itemsPerPage;
  		$scope.totalItems = $scope.test.length;
  		$scope.slicetmp="begin:"+begin+", eng: "+end+", total:"+$scope.test.length;
  		$scope.showedTranslates = $scope.test.slice(begin, end);
  	};
   	$scope.$watch("[currentPage , itemsPerPage , test]", sliceResult , true);

  	//$scope.pageChanged = function() {};

}]);


swissKnifeControllers.controller('calculatorCtrl', function ($scope) {
	$scope.inputNum=[];
	$scope.inputOps=[];
	$scope.displayNum=0;
	$scope.records=[];
	$scope.buttons = [7,8,9,"+",4,5,6,"-",1,2,3,"X","C",0,"=","/"];
	var currentNum=0;
	$scope.apply_input=function(btn){

		if(btn>=0 && btn <=9){//input is number
			
			if (currentNum==0||null)
			{
				currentNum=btn;
			}
			else{
				currentNum=currentNum*10+btn;
			}

		}
		else if(btn=="C"){
				currentNum=0;
				$scope.inputNum=[];
				$scope.inputOps=[];
		}
		else if(currentNum!=null){//input is operation
			
			if (btn=="="){
				calculate();
			}
			else{
				$scope.inputOps.push(btn);
				if(currentNum==null){
					currentNum=0;
				}
				$scope.inputNum.push(currentNum);
				currentNum=null; //prevent consucutive operators
			}
		}
		$scope.displayNum="";//display current inputs
		 for (var i=0;i<$scope.inputNum.length;i++){
		 	$scope.displayNum+=$scope.inputNum[i];
		 	if($scope.inputOps[i]){$scope.displayNum+=$scope.inputOps[i];}
		 }
		 if($scope.inputNum.length==0||currentNum!=null){$scope.displayNum+=currentNum;}		
	};
	var calculate=function(){
		var result;
		var record;
		$scope.inputNum.push(currentNum);
		for(var i=0;i<$scope.inputNum.length;i++){
			if($scope.inputOps[i-1]){
				switch($scope.inputOps[i-1]){
					case "+": result+=$scope.inputNum[i];break;
					case "-": result-=$scope.inputNum[i];break;
					case "X": result*=$scope.inputNum[i];break;
					case "/": result/=$scope.inputNum[i];break;
				}
			}
			else{
				result=$scope.inputNum[i];
			}
			
		}
		currentNum=result;
		record=$scope.displayNum+"="+result;
		$scope.records.push(record);
		$scope.inputNum=[];
		$scope.inputOps=[];
	}
});