var swissKnife = angular.module('swissKnife', [
  'ngRoute',
  'swissKnifeControllers',
  'restangular',
  'dictionaryServices',
  'ui.bootstrap',
  'swissKnifeAnimate',
  'smoothScroll'
]);

swissKnife.config(['$routeProvider', '$locationProvider', '$httpProvider', 'RestangularProvider',
	function($routeProvider, $locationProvider ,$httpProvider,RestangularProvider) {
		$routeProvider.
		when("/home",{
			templateUrl: "partial/home.html",
			controller: ""
		}).
		when("/dictionary",{
			templateUrl: "partial/dictionary.html",
			controller: "dictionaryCtrl"
		}).
		when("/calculator",{
			templateUrl: "partial/calculator.html",
			controller: "calculatorCtrl"
		}).
		otherwise({
			redirectTo:"/home"
		});
		//this can get rid of the "#" in url but requires server side config
		// $locationProvider.html5Mode(true);

		RestangularProvider.setBaseUrl('http://www.dictionaryapi.com/api/v1/references/collegiate/xml');
      	RestangularProvider.setDefaultRequestParams({ key: 'b926111a-e0e0-4357-a0c6-562f8be365a3' })
      	// RestangularProvider.setRestangularFields({
      	// 	id: '_id.$oid'
      	// });

		//$httpProvider.defaults.xsrfCookieName = 'csrftoken';
    	//$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';{
  		//$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

	}]);

//Validation
var ISENG_REGEXP = /^[a-zA-Z]+$/;
var NOTCHN_REGEXP = /[^\u4e00-\u9fa5]/;

swissKnife.directive('valida', function() {
	return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			attrs.$observe('valida', function(value){
			ctrl.$validators.valida = function(modelValue, viewValue) {
				if (ctrl.$isEmpty(modelValue)) {
          // consider empty models to be valid
          			return true;
      			}

      			if(value==="Chinese"&&!NOTCHN_REGEXP.test(viewValue)){ 
		          // it is valid
		         	return true;
	      		}
	      		else if(value==="English"&&ISENG_REGEXP.test(viewValue)){
	      			return true;
	      		}

        // it is invalid
        return false;
			};
		});
		}
	};
});