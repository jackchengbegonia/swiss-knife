$(document).ready(function(){
  $.getJSON("https://bootswatch.com/api/3.json", function (data) {
    var themes = data.themes;
    var select = $("#bootswatch");
    var list   = [];
    select.show();
  list.push('<option value="0">Theme</option>');
  themes.forEach(function(value, index){
    list.push('<option value="' + index + '">' + value.name + '</option>');    
  });
  select.html(list.join(''));
  select.selectpicker('mobile');
  select.selectpicker('refresh');
  select.change(function(){
    var theme = themes[$(this).val()];
    $("link").eq(2).attr("href", theme.css);
    //after change theme top mobile menu auto collapse
    $('#navbar-top-collapse').removeClass('in')
  }).change();

}, "json").fail(function(){
  });
});

